/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   minishell.h                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: qmanamel <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/07/30 13:00:38 by qmanamel          #+#    #+#             */
/*   Updated: 2017/08/18 10:56:14 by qmanamel         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef MINISHELL_H
# define MINISHELL_H

# include <stdio.h>
# include <stdlib.h>
# include <string.h>
# include <unistd.h>
# include <sys/types.h>
# include <sys/stat.h>
# include <signal.h>
# include <fcntl.h>
# include <termios.h>
# include <term.h>
# include <curses.h>
# include <dirent.h>
# include <pwd.h>
# include <sys/wait.h>
# include "../libft/libft.h"
# define BUFF_SIZE 1024
# define CLEAR "\e[1;1H\e[2J"
# define SETENV_SYNTAX "\e[0;1m\e[0;32msyntax error: [Key] = [Value]\e[0;0m"
# define ENV_ERROR_1 "\e[0;31mNot found , try [setenv]\e[1;0m"
# define FILE_IN 1
# define FILE_OUT 2
# define FILE_APPEND 3
# define CMD_SUCCESS 1
# define CMD_FALUIRE 0

typedef struct  s_red
{
    char        **command;
    char        *file;
    int         file_type;
}               t_red;

typedef struct s_logicalop
{
    int         ret_val;
}              t_logicalop;

char	    **g_env;
t_red       *main_red;
t_logicalop *logic_op;

struct termios oterm;
struct termios term;

void	prog_handle(int sign_n);
void	proc_handler(int sign_n);
void	exit_shell(void);
void	exec_commang_env(void);
void	new_env();
void	display_prompt(void);
int		pipe_parse(char *command);
int 	exec_pipe(char **str, int len);
char	*ft_getenv(char *s_envstr);
int		exec_command(char **cmd);
int		parse_input(char **input);
int		exec_command_main(char **command);
int		run_cmds(char **commands);
void	exec_cd(char *dir, int old_pwd_bool);
void    execve_pipe(char ***cmd);
void	exit_handler(int n);
int     red_parse(char *str);
int 	exec_red(char **str, int len);
int		exec_command_cd(char **dir);
int		exec_command_echo(char **line);
int		exec_shell_cmd(char *path, char **args);
int		exec_command_unset(char *str);
int		exec_command_setenv(char **g_env);
void	set_env(char *key, char *value);
int     logical_op_parse(char *str);

#endif
