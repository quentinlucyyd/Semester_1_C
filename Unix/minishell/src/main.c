/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: qmanamel <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/07/30 13:00:24 by qmanamel          #+#    #+#             */
/*   Updated: 2017/07/30 14:12:05 by qmanamel         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../includes/minishell.h"

int			main(int c, char **v, char **envv)
{
	char	*input;
	int		ret;
	char	**commands;

	new_env(c, v, envv);
	logic_op = (t_logicalop *)malloc(sizeof(t_logicalop));	
	ft_putstr(CLEAR);
	while (42)
	{
		display_prompt();
		signal(SIGINT, prog_handle);
		parse_input(&input);
		input = ft_strtrim(input);
		if (ft_isemptystr(input) || !ft_strcmp(input, ";"))
		{
			continue ;
			free(input);
		}
		commands = ft_strsplit(input, ';');
		free(input);
		ret = run_cmds(commands);
		ft_freestrarr(commands);
		if (!ret)
			exit_shell();
	}

	ft_freestrarr(g_env);
	return (0);
}
