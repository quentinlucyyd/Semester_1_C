/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   signals.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: qmanamel <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/08/15 09:23:52 by qmanamel          #+#    #+#             */
/*   Updated: 2017/08/15 09:24:04 by qmanamel         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../includes/minishell.h"

void	prog_handle(int sign_n)
{
	(void)sign_n;
	ft_putchar('\n');
	display_prompt();
}

void	exit_handler(int n)
{
	if (n == SIGINT)
	{		
		tcsetattr(0, TCSANOW, &oterm);
		exit(EXIT_SUCCESS);
	}
}

void	proc_handler(int sign_n)
{
	(void)sign_n;
	ft_putendl("\n");
}
