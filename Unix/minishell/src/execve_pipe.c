/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   execve_pipe.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: qmanamel <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/08/19 13:36:40 by qmanamel          #+#    #+#             */
/*   Updated: 2017/08/19 13:36:41 by qmanamel         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../includes/minishell.h"

/*
	Does not Work With excve() but with execvp()
*/

char	**ft_strarrdup(char **str)
{
	int 	i;
	char 	**f_str;

	i = -1;
	while (str[++i])
		i++;
	f_str = (char **)malloc(sizeof(char *) * (i + 1));
	i = -1;
	while (str[++i])
		f_str[i] = ft_strdup(str[i]);
	return(f_str);
}

void    execve_pipe(char ***cmd) 
{
	int   p[2];
	pid_t pid;
	int   fd_in = 0;

	while (*cmd != NULL)
	{
		pipe(p);
		pid = fork();
		if (pid == 0)
		{
			dup2(fd_in, 0);
			if (*(cmd + 1) != NULL)
				dup2(p[1], 1);
            close(p[0]);
			exec_command_main((*cmd));
			exit(EXIT_FAILURE);
		}
		else
		{
			wait(&pid);
			close(p[1]);
			fd_in = p[0];
			cmd++;
		}
	}
}

int 		exec_pipe(char **str, int len)
{
	char 	***pipes;
	char	**temp_pipe;
	int 	i;

	pipes = (char ***)malloc(sizeof(char **) * (len + 1));
	i = -1;
	while(str[++i])
	{
		temp_pipe = ft_strsplit(str[i], ' ');
		pipes[i] = ft_strarrdup(temp_pipe);
	}
	pipes[i] = NULL;
	execve_pipe(pipes);
	ft_freestrarr(temp_pipe);
	i = -1;
	while (pipes[++i])
		ft_freestrarr(pipes[i]);
	return(1);
}

int			pipe_parse(char *command)
{
	char	**temp_pipes;
	int 	len;

	len = 0;
	temp_pipes = ft_strsplit_num(command, '|', &len);
	exec_pipe(temp_pipes, len);
	ft_freestrarr(temp_pipes);
	return (1);
}