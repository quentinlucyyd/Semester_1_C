/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   _help_functions.c                                  :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: qmanamel <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/07/30 12:59:06 by qmanamel          #+#    #+#             */
/*   Updated: 2017/08/18 08:01:00 by qmanamel         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../includes/minishell.h"

void		new_env(int c, char **v, char **envv)
{
	int		len;
	int		i;

	(void)c;
	(void)v;
	len = -1;
	i = -1;
	while (envv[++len])
		len++;
	g_env = (char **)malloc(sizeof(char *) * (len + 1));
	while (envv[++i])
		g_env[i] = ft_strdup(envv[i]);
	g_env[i] = NULL;
}

char		*ft_getenv(char *s_envstr)
{
	int		i;
	char	*temp;

	i = -1;
	while (g_env[++i])
	{
		temp = ft_strjoin(s_envstr + 1, "=");
		if (ft_strstartswith(g_env[i], temp))
		{
			free(temp);
			return (ft_strchr(g_env[i], '=') + 1);
		}
		free(temp);
	}
	return (NULL);
}

void		exec_commang_env(void)
{
	int		i;

	i = -1;
	while (g_env[++i])
		ft_putendl(g_env[i]);
}
