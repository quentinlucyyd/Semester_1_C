/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   unset_builtin.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: qmanamel <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/07/30 13:00:05 by qmanamel          #+#    #+#             */
/*   Updated: 2017/07/30 13:00:08 by qmanamel         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../includes/minishell.h"

static 	int 		ft_envdoesnotexist(char *str)
{
	int i;
	int len;

	len = -1;
	i = -1;
	while(g_env[++len])
		len++;
	while (g_env[++i])
	{
		if (ft_strstartswith(g_env[i], str))
			return(0);
		else
			i++;
	}
	return(1);
}

int			exec_command_unset(char *str)
{
	int		i;
	int		j;
	char	**r_env;

	j = 0;
	i = -1;
	if (!str)
		return (0);
	if(!ft_envdoesnotexist(str))
		return (0);
	while (g_env[++i])
		i++;
	r_env = (char **)malloc(sizeof(char *) * i);
	i = -1;
	while (g_env[++i])
	{
		if (ft_strstartswith(g_env[i], str))
			++i;
		else
		{
			r_env[j] = ft_strdup(g_env[i]);
			j++;
		}
	}
	ft_freestrarr(g_env);
	g_env = r_env;
	return (1);
}
