/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   logical_op.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: qmanamel <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/09/04 08:21:31 by qmanamel          #+#    #+#             */
/*   Updated: 2017/09/04 08:21:46 by qmanamel         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../includes/minishell.h"

int logical_op_and(char **cmd_1, char **cmd_2)
{
	int 	ret;

    printf("REACH 1\n");
	logic_op->ret_val = 0;
    ret = run_cmds(cmd_1);
    printf("REACH 2\n");
	if (!logic_op->ret_val)
		ret = run_cmds(cmd_2);
	ft_freestrarr(cmd_1);
    return(ret);
}

int logical_op_or(char **cmd_1, char **cmd_2)
{
	int 	ret;

	logic_op->ret_val = 0;
    ret = run_cmds(cmd_1);
	if (logic_op->ret_val)
		ret = run_cmds(cmd_2);
	ft_freestrarr(cmd_1);
    return(ret);
}

int     ft_find_len(char *str)
{
    int     len;
    int     bool_int;

    bool_int = 0;
    len = 0;
    while(str[len] && bool_int == 0)
    {
        if (str[len] == '|' && str[len + 1] == '|')
            bool_int = 1;
        else if (str[len] == '&' && str[len + 1] == '&')
            bool_int = 1;
        else
            len++;
    }
    if (str[len + 2] == '&' || str[len + 2] == '|')
    {
        ft_putstr("42sh: parse error near `");
        ft_putchar(str[len + 2]);
        ft_putstr("`\n");
        return(0);
    }
    return(len);
}

int     log_pars(char *str, char **f_cmd, char **s_cmd, int len)
{
    *f_cmd = (char *)malloc(sizeof(char) * (len));
    *f_cmd = ft_strncpy(*f_cmd, str, len);
    *s_cmd = (char *)malloc(sizeof(char) * (ft_strlen(str) - (len + 3)));
    *s_cmd = ft_strdup(str + (len + 2));
    *s_cmd = ft_strtrim(*s_cmd);
    return(1);
}

int     logical_op_parse(char *str)
{
    int     len;
    int     ret;
    char    *s_cmd;
    char    *f_cmd;
    char    **cmd_2;
    char    **cmd_1;

    ret = 1;
    cmd_1 = (char **)malloc(sizeof(char *) * 2);
    cmd_2 = (char **)malloc(sizeof(char *) * 2);
    if (!(len = ft_find_len(str)))
        return (1);
    if (!log_pars(str, &f_cmd, &s_cmd, len))
        return(1);
    printf("F_CMD : %s\n", f_cmd);
    printf("S_CMD : %s\n", s_cmd);
    cmd_1[0] = ft_strdup(f_cmd);
    cmd_2[1] = NULL;
    cmd_2[0] = ft_strdup(s_cmd);
    cmd_2[1] = NULL;
    int c1= -1;
    while (cmd_1[++c1])
        printf("%s\n", cmd_1[c1]);
    printf("REACH\n");
    c1 = -1;
    while (cmd_2[++c1])
        printf("%s\n", cmd_2[c1]);
    /*if (str[len + 1] == '&')
        ret = logical_op_and(cmd_1, cmd_2);
    else if (str[len + 1] == '|')
        ret = logical_op_or(cmd_1, cmd_2);*/
    return (ret);
}