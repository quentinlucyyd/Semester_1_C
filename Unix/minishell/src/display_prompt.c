/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   display_prompt.c                                   :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: qmanamel <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/09/05 12:21:01 by qmanamel          #+#    #+#             */
/*   Updated: 2017/09/05 12:21:03 by qmanamel         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../includes/minishell.h"

void		display_prompt(void)
{
	char 	*dir;
	char 	buff[BUFF_SIZE];
	dir = getcwd(buff, BUFF_SIZE);
	dir = ft_strrchr(dir, '/');
	ft_putstr("\x1b[1m-> : \x1b[32m(");
	ft_putstr(dir + 1);
	ft_putstr(")\x1b[31m % \x1b[0m");
}
