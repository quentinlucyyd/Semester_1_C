/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   execve_red.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: qmanamel <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/08/25 08:50:24 by qmanamel          #+#    #+#             */
/*   Updated: 2017/08/25 08:50:44 by qmanamel         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../includes/minishell.h"

void    execve_red(char **command, char *file, int opt)
{
    pid_t   pid;
    int     fd;

    fd = 0;
    pid = fork();
    if (opt == 1)
        fd = creat(file, 0644);
    if (pid == -1)
        ft_putendl("Error in Forking");
    else if (pid == 0)
    {
        dup2(fd, 1);
        close(fd);
        exec_command_main(command);
        exit(EXIT_SUCCESS);
    }
}

char    *get_red_command(char *str, char **files, int *opt)
{
    int     i;
    int     stop;
    int     len;
    char    *args;
    char    *command;

    args = NULL;
    if (ft_strstr(str, "<"))
    {
        args = ft_strchr(str, '<');
        *opt = FILE_OUT;
    }
    else
    {
        args = ft_strchr(str,'>');
        *opt = FILE_IN;
    }
    printf("ARGS : %s\n", args);
    command = NULL;
    len = 0;;
    stop = 1;
    i = 0;
    while (str[i] && stop)
    {
        if (str[i] == '>' || str[i] == '<')
            stop = 0;
        else
            len++;
        i++;
    }

    args = ft_strtrim(args + 1);
    *files = ft_strdup(args);
    command = (char *)malloc(sizeof(char) * (len + 1));
    command = ft_strncpy(command, str, len);
    return(command);
}

int     red_parse(char *str)
{
    char    *command;
    char    **f_command;
    int     opt;
    char    *file;

    opt = 0;
    command = get_red_command(str, &file, &opt);
    printf("OPT IS : %d\n", opt);
    f_command = ft_strsplit(command, ' ');
    execve_red(f_command, file, opt);
    return (1);
}