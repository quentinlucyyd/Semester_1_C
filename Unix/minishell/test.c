#include <stdio.h>
#include <stdlib.h>
#include "libft/libft.h"

char    **ft_log_check(char *str)
{
    int     len;
    char    *ret;
    char    *sec;
    int     bool_int;
    char    **temp_ret;

    bool_int = 0;
    len = -1;
    temp_ret = (char **)malloc(sizeof(char *) * 3);
    while(str[++len] && !bool_int)
    {
        if (str[len] == '|' && str[len + 1] == '|')
            bool_int = 1 ;
        else if (str[len] == '&' && str[len + 1] == '&')
            bool_int = 1;
        else
            len++;
    }
    len--;
    sec = ft_strdup(str + (len + 2));
    ret = (char *)malloc(sizeof(char) * len);
    ft_strncpy(ret, str, len);
    temp_ret[0] = ft_strdup(ret);
    temp_ret[1] = ft_strtrim(sec);
    temp_ret[2] = NULL;
    free(sec);
    free(ret);
    return(temp_ret);
}

int main(int c, char **v)
{
    (void)c;
    char **command;
    command = ft_log_check(v[1]);
    int i = -1;
    while (command[++i])
        printf("%s\n", command[i]);
    ft_freestrarr(command);
}